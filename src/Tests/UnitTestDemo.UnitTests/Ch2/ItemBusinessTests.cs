﻿using Ch1;
using Xunit;

namespace UnitTestDemo.Tests.Ch2
{
    public class ItemBusinessTests
    {
        [Theory]
        [InlineData(ItemType.Iphone, 1)]
        [InlineData(ItemType.Samsung, 1)]
        [InlineData(ItemType.Iphone, 6)]
        [InlineData(ItemType.Samsung, 6)]
        public void GetExtendPrices_InputMultipleData_OutputCorrectPrices(ItemType itemType, int qty)
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            var (item, expected) = GetTestData(itemType, qty);

            //act
            var actual = itemBusiness.GetExtendPrices(item, qty);

            //assert
            Assert.Equal(expected, actual);
        }

        public (string, decimal) GetTestData(ItemType itemType, int qty = 1)
        {
            var item = string.Empty;
            var prices = 0m;

            switch (itemType)
            {
                case ItemType.Iphone:
                    item = "Iphone";
                    prices = 10m;
                    break;
                case ItemType.Samsung:
                    item = "Samsung";
                    prices = 0m;
                    break;
            }

            return (item, prices * qty);
        }

        public enum ItemType
        {
            Iphone,
            Samsung
        }

        [Fact]
        public void GetDefaultPrice_InputIphone_OutputTenPrices()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            var (item, expected) = GetTestData(ItemType.Iphone);

            //act
            var actual = itemBusiness.GetDefaultPrice(item);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetDefaultPrice_InputSamsung_OutputZeroPrice()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            var (item, expected) = GetTestData(ItemType.Samsung);

            //act
            var actual = itemBusiness.GetDefaultPrice(item);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrices_InputIphoneAndQtyIsSix_OutputSixtyPrices()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            const int qty = 6;
            var (item, expected) = GetTestData(ItemType.Iphone, qty);

            //act
            var actual = itemBusiness.GetExtendPrices(item, qty);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrices_InputSamsungAndQtyIsSix_OutputZeroPrice()
        {
            //arrange
            var itemBusiness = new ItemBusiness();
            const int qty = 6;
            var (item, expected) = GetTestData(ItemType.Samsung, qty);

            //act
            var actual = itemBusiness.GetExtendPrices(item, qty);

            //assert
            Assert.Equal(expected, actual);
        }
    }
}