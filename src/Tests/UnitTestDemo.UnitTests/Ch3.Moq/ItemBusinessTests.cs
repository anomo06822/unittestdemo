﻿using System.Collections.Generic;
using Moq;
using UnitTestDemo.Ch3;
using UnitTestDemo.Model;
using Xunit;

namespace UnitTestDemo.Tests.Ch3.Moq
{
    public class ItemBusinessTests
    {
        private Mock<IPriceCalculator> _priceCalculator;
        private Mock<IItemDA> _itemDa;

        [Fact]
        public void GetAppleItems_AppleItemTwoCounts_ReturnCorrectValues()
        {
            //arrange
            var items = new List<Item>
            {
                new Item {Label = "Apple", ItemNumber = "1"},
                new Item {Label = "Samsung", ItemNumber = "2"},
                new Item {Label = "Apple", ItemNumber = "3"}
            };

            var expected = new List<Item>
            {
                new Item {Label = "Apple", ItemNumber = "1"},
                new Item {Label = "Apple", ItemNumber = "3"}
            };

            _priceCalculator = new Mock<IPriceCalculator>();
            _itemDa = new Mock<IItemDA>();
            _itemDa.Setup(x => x.GetItems()).Returns(items);
            var itemBusiness = new ItemBusiness(_priceCalculator.Object, _itemDa.Object);

            //act
            var actual = itemBusiness.GetAppleItems();

            //assert
            _itemDa.Verify(x => x.GetItems(), Times.Once);
            ;
            Assert.Equal(expected.Count, actual.Count);
            Assert.Equal(expected[0].ItemNumber, actual[0].ItemNumber);
            Assert.Equal(expected[0].Label, actual[0].Label);
            Assert.Equal(expected[1].ItemNumber, actual[1].ItemNumber);
            Assert.Equal(expected[1].Label, actual[1].Label);
        }


        [Fact]
        public void GetAppleItems_itemsIsNullOrEmpty_ReturnNull()
        {
            //arrange
            _priceCalculator = new Mock<IPriceCalculator>();
            _itemDa = new Mock<IItemDA>();
            var itemBusiness = new ItemBusiness(_priceCalculator.Object, _itemDa.Object);

            //act
            var actual = itemBusiness.GetAppleItems();

            //assert
            _itemDa.Verify(x => x.GetItems(), Times.Once);
            ;
            Assert.Null(actual);
        }

        [Fact]
        public void GetDefaultPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            var expected = 1m;
            _priceCalculator = new Mock<IPriceCalculator>();
            _priceCalculator.Setup(x => x.GetDefaultPrice(It.IsAny<string>())).Returns(1m);
            _itemDa = new Mock<IItemDA>();
            var itemBusiness = new ItemBusiness(_priceCalculator.Object, _itemDa.Object);

            //act
            var actual = itemBusiness.GetDefaultPrice(string.Empty);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetExtendPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            var expected = 0m;
            _priceCalculator = new Mock<IPriceCalculator>();
            _priceCalculator.Setup(x => x.GetExtendPrices(It.IsAny<string>(), It.IsAny<int>())).Returns(0m);
            _itemDa = new Mock<IItemDA>();
            var itemBusiness = new ItemBusiness(_priceCalculator.Object, _itemDa.Object);

            //act
            var actual = itemBusiness.GetExtendPrices(string.Empty, 0);

            //assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetItems_CheckDependOnItemDA_CallOneTime()
        {
            //arrange
            _priceCalculator = new Mock<IPriceCalculator>();
            _itemDa = new Mock<IItemDA>();
            var itemBusiness = new ItemBusiness(_priceCalculator.Object, _itemDa.Object);

            //act
            itemBusiness.GetItems();

            //assert
            _itemDa.Verify(x => x.GetItems(), Times.Once);
        }
    }
}