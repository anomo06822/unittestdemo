﻿using System;
using UnitTestDemo.Ch3.TestDouble;

namespace UnitTestDemo.Tests.Ch3.Fake
{
    public class FakeTimeWrapper : ITimeWrapper
    {
        internal DateTime MockTime;
        public DateTime Now => MockTime;
    }
}