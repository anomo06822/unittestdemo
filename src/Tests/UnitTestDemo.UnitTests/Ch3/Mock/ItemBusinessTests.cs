﻿using NSubstitute;
using UnitTestDemo.Ch3.TestDouble;
using Xunit;

namespace UnitTestDemo.Tests.Ch3.Mock
{
    /// <summary>
    ///     Mock 的範例，主要是驗證互動行為，所以這邊採用 Received(1) 的方式處理，會發現我們關注的是 PriceCalculator 是否被使用
    /// </summary>
    public class ItemBusinessTests
    {
        private IPriceCalculator _priceCalculator;
        private IPriceValidator _priceValidator;

        [Fact]
        public void GetDefaultPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceValidator = Substitute.For<IPriceValidator>();
            _priceCalculator.GetDefaultPrice(Arg.Any<string>()).Returns(0m);
            var itemBusiness = new ItemBusiness(_priceCalculator, _priceValidator);
            //act
            itemBusiness.GetDefaultPrice(string.Empty);

            //assert
            _priceCalculator.Received(1).GetDefaultPrice(string.Empty);
        }

        [Fact]
        public void GetExtendPrice_CheckDependOnPriceCalculator_CallOneTime()
        {
            //arrange
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceValidator = Substitute.For<IPriceValidator>();
            _priceCalculator.GetExtendPrice(Arg.Any<string>(), Arg.Any<int>()).Returns(0m);

            var itemBusiness = new ItemBusiness(_priceCalculator, _priceValidator);
            //act
            itemBusiness.GetExtendPrice(string.Empty, 0);

            //assert
            _priceCalculator.Received(1).GetExtendPrice(string.Empty, 0);
        }
    }
}