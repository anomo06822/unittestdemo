﻿using NSubstitute;
using UnitTestDemo.Ch3.TestDouble;
using Xunit;

namespace UnitTestDemo.Tests.Ch3.Dummy
{
    /// <summary>
    ///     Dummy 的範例，主要 Dummy 只是過程中的需要的步驟，對於 Return 的資訊並不重視
    /// </summary>
    public class ItemBusinessTests
    {
        private IPriceCalculator _priceCalculator;
        private IPriceValidator _priceValidator;

        [Theory]
        [InlineData(5, 5)]
        [InlineData(0, 0)]
        public void ValidatePrice_PriceIsGreaterThanOrEqualToZero_ReturnValue(decimal price, decimal expected)
        {
            //arrange
            _priceCalculator = Substitute.For<IPriceCalculator>();
            _priceValidator = Substitute.For<IPriceValidator>();
            var item = _priceCalculator.GetDefaultPrice("Test").Returns(price);
            var itemBusiness = new ItemBusiness(_priceCalculator, _priceValidator);

            //act
            var actual = itemBusiness.GetDefaultPrice("Test");

            //assert
            Assert.Equal(expected, actual);
        }
    }
}