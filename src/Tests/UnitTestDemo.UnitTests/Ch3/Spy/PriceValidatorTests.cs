﻿using System.IO;
using UnitTestDemo.Ch3.TestDouble;
using Xunit;

namespace UnitTestDemo.Tests.Ch3.Spy
{
    /// <summary>
    ///     spy 的範例，這邊為了跟 mock 區隔，會發現多了 SpyLogger 必且多了一些屬性進行記錄，而後面我們 assert 的時間就可以透過這方式驗證是否有互動
    /// </summary>
    public class PriceValidatorTests
    {
        private IPriceCalculator _priceCalculator;

        [Fact]
        public void ValidatePrice_PriceIsLessThanZero_WriteLog()
        {
            //arrange
            var price = -1m;
            var spyLogger = new SpyLogger();
            var priceValidator = new PriceValidator(spyLogger);

            //act
            //assert
            Assert.Throws<InvalidDataException>(() => priceValidator.ValidatePrice(price));
            Assert.True(spyLogger.IsWrittenLog());
        }
    }
}