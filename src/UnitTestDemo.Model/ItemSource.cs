﻿using System.Collections.Generic;

namespace UnitTestDemo.Model
{
    public class ItemSource
    {
        public List<Item> Items { get; set; }
    }
}