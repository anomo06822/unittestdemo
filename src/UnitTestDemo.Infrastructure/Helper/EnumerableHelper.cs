﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnitTestDemo.Infrastructure.Extension;

namespace UnitTestDemo.Infrastructure.Helper
{
    public class EnumerableHelper
    {
        public static IEnumerable<T> Where<T>(IEnumerable<T> sources, Func<T, bool> predicate)
        {
            if (sources.IsNullOrEmpty() || predicate == null) return sources;

            return sources.Where(predicate);
        }
    }
}