﻿using System.Collections.Generic;
using System.Linq;

namespace UnitTestDemo.Infrastructure.Extension
{
    public static class EnumerableExtension
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> sources)
        {
            return sources == null || sources.Any() == false;
        }
    }
}