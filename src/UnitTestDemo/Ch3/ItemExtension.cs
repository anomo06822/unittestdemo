﻿using UnitTestDemo.Model;

namespace UnitTestDemo.Ch3
{
    public static class ItemExtension
    {
        public static bool CompareItemNumber(this Item item, string itemNumber)
        {
            return SelectorItemHelper.CompareItemNumber(item, itemNumber);
        }
    }
}