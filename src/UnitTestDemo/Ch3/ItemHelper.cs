﻿using UnitTestDemo.Infrastructure.Extension;
using UnitTestDemo.Model;

namespace UnitTestDemo.Ch3
{
    public class ItemHelper
    {
        public static bool IsAppleLabel(Item item)
        {
            return IsLabel(item, LabelConst.Apple);
        }

        public static bool IsSamsungLabel(Item item)
        {
            return IsLabel(item, LabelConst.Samsung);
        }

        public static bool IsLabel(Item item, string label)
        {
            return item.IsNotNull() && item.Label == label;
        }
    }
}