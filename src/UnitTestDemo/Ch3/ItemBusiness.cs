﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnitTestDemo.Infrastructure.Helper;
using UnitTestDemo.Model;

namespace UnitTestDemo.Ch3
{
    public class ItemBusiness
    {
        private readonly IItemDA _itemDa;
        private readonly IPriceCalculator _priceCalculator;

        public ItemBusiness(IPriceCalculator priceCalculator, IItemDA itemDa)
        {
            _priceCalculator = priceCalculator;
            _itemDa = itemDa;
        }

        public decimal GetDefaultPrice(string item)
        {
            return _priceCalculator.GetDefaultPrice(item);
        }

        public decimal GetExtendPrices(string item, int qty)
        {
            return _priceCalculator.GetExtendPrices(item, qty);
        }

        public List<Item> GetAllItems()
        {
            return GetItems();
        }

        public List<Item> GetAppleItems()
        {
            return GetItems(ItemHelper.IsAppleLabel);
        }

        public List<Item> GetSamsungItems()
        {
            return GetItems(ItemHelper.IsSamsungLabel);
        }

        public List<Item> GetItems(Func<Item, bool> predicate = null)
        {
            var items = _itemDa.GetItems();
            return EnumerableHelper.Where(items, predicate)?.ToList();
        }
    }
}