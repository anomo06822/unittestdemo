﻿namespace UnitTestDemo.Ch3.TestDouble
{
    public interface IPriceValidator
    {
        bool ValidatePrice(decimal price);
    }
}