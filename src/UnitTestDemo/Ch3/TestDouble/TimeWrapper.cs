﻿using System;

namespace UnitTestDemo.Ch3.TestDouble
{
    public class TimeWrapper : ITimeWrapper
    {
        public DateTime Now => DateTime.Now;
    }
}