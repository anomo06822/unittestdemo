﻿using System.IO;

namespace UnitTestDemo.Ch3.TestDouble
{
    public class PriceValidator : IPriceValidator
    {
        private readonly ILogger _logger;

        public PriceValidator()
        {
            _logger = new Logger();
        }

        public PriceValidator(ILogger logger)
        {
            _logger = logger;
        }

        public bool ValidatePrice(decimal price)
        {
            if (price >= 0) return true;

            _logger.WriteLog("price is less than zero");
            throw new InvalidDataException("price is less than zero");
        }
    }
}