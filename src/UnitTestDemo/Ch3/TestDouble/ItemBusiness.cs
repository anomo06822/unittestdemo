﻿using System;

namespace UnitTestDemo.Ch3.TestDouble
{
    public class ItemBusiness
    {
        private readonly IPriceCalculator _priceCalculator;
        private readonly IPriceValidator _priceValidator;

        public ItemBusiness(IPriceCalculator priceCalculator)
        {
            _priceCalculator = priceCalculator;
            _priceValidator = new PriceValidator();
        }

        public ItemBusiness(IPriceCalculator priceCalculator, IPriceValidator priceValidator)
        {
            _priceCalculator = priceCalculator;
            _priceValidator = priceValidator;
        }

        public decimal GetDefaultPrice(string item)
        {
            return GetPrice((x, _) => _priceCalculator.GetDefaultPrice(x), item);
        }

        public decimal GetExtendPrice(string item, int qty)
        {
            return GetPrice((x, y) => _priceCalculator.GetExtendPrice(x, y), item, qty);
        }

        private decimal GetPrice(Func<string, int, decimal> predicate, string item, int qty = 1)
        {
            var price = predicate(item, qty);

            _priceValidator.ValidatePrice(price);

            return price;
        }
    }
}