﻿using System;

namespace UnitTestDemo.Ch3.TestDouble
{
    public class DateTimeProvider
    {
        public ITimeWrapper TimeWrapper = new TimeWrapper();

        public DateTime GetCurrentTime()
        {
            return TimeWrapper.Now;
        }
    }
}