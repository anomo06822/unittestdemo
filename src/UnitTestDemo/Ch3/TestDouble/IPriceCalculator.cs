﻿namespace UnitTestDemo.Ch3.TestDouble
{
    public interface IPriceCalculator
    {
        decimal GetDefaultPrice(string item);
        decimal GetExtendPrice(string item, int qty);
    }
}