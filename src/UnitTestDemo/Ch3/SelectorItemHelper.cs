﻿using System;
using UnitTestDemo.Model;

namespace UnitTestDemo.Ch3
{
    public class SelectorItemHelper
    {
        public static Func<Item, string, bool> CompareItemNumber = (item, itemNumber) => item?.ItemNumber == itemNumber;
    }
}