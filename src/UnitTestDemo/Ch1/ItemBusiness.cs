﻿namespace Ch1
{
    public class ItemBusiness
    {
        public const string Iphone = "Iphone";

        public decimal GetDefaultPrice(string item)
        {
            return item == Iphone ? 10m : 0m;
        }

        public decimal GetExtendPrices(string item, int qty)
        {
            return GetDefaultPrice(item) * qty;
        }
    }
}